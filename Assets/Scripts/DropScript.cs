﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropScript : MonoBehaviour, IDropHandler {

    public bool filled = false;
	
	#region IDropHandler implementation

	public void OnDrop (PointerEventData eventData)
	{
        if (!filled)
        {
            DragHandler.itemBeingDragged.transform.SetParent(transform);
            transform.GetComponent<PuzzlePiece>().puzzleName = DragHandler.itemBeingDragged.name.ToString().Replace("(Clone)", "");
            filled = true;
        }
    }

	#endregion



}
