﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerObject : NetworkBehaviour
{
    public GameObject playerUnitPrefab;
    // Start is called before the first frame update
    void Start()
    {
        if (!isLocalPlayer)
            return;

        CmdSpawnMyUnit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [Command]
    void CmdSpawnMyUnit()
    {
        GameObject go = Instantiate(playerUnitPrefab);
        NetworkServer.Spawn(go);
        print("Success");
    }
}
