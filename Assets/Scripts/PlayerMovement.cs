﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private static PlayerMovement _instance;

    public static PlayerMovement Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public GameObject Player;

    public Vector3 playerStartPos;

    private IEnumerator coroutine;

    public List<string> ExecutionList = new List<string>();

    public bool won = false;


    public void MoveLeft()
    {
        Vector3 source = Player.transform.position;
        Vector3 target = source + Vector3.left * 4;

        coroutine = MovePlayer(source, target);
        StartCoroutine(coroutine);
    }

    public void MoveRight()
    {
        Vector3 source = Player.transform.position;
        Vector3 target = source - Vector3.left * 4;

        coroutine = MovePlayer(source, target);
        StartCoroutine(coroutine);
    }

    public void MoveUp()
    {
        Vector3 source = Player.transform.position;
        Vector3 target = source + Vector3.forward * 4;

        coroutine = MovePlayer(source, target);
        StartCoroutine(coroutine);
    }

    public void MoveDown()
    {
        Vector3 source = Player.transform.position;
        Vector3 target = source - Vector3.forward * 4;

        coroutine = MovePlayer(source, target);
        StartCoroutine(coroutine);
    }


    public void Execute()
    {
        StartCoroutine(ReadExecutionList());
    }


    IEnumerator ReadExecutionList()
    {
        for(int i = 0; i < ExecutionList.Count; ++i)
        {
            if(ExecutionList[i] == "Left")
            {
                Vector3 source = Player.transform.position;
                Vector3 target = source + Vector3.left * 4;

                coroutine = MovePlayer(source, target);
                yield return StartCoroutine(coroutine);
            }

            else if (ExecutionList[i] == "Right")
            {
                Vector3 source = Player.transform.position;
                Vector3 target = source - Vector3.left * 4;

                coroutine = MovePlayer(source, target);
                yield return StartCoroutine(coroutine);
            }

            else if (ExecutionList[i] == "Up")
            {
                Vector3 source = Player.transform.position;
                Vector3 target = source + Vector3.forward * 4;

                coroutine = MovePlayer(source, target);
                yield return StartCoroutine(coroutine);
            }

            else if (ExecutionList[i] == "Down")
            {
                Vector3 source = Player.transform.position;
                Vector3 target = source - Vector3.forward * 4;

                coroutine = MovePlayer(source, target);
                yield return StartCoroutine(coroutine);
            }
        }

        if(!won)
        {
            PlayerMovement.Instance.SetStartPos();
            UIManager.Instance.ClearAll();
            UIManager.Instance.AlertError("You could not reach goal! Try again");
            
        }
    }


    IEnumerator MovePlayer(Vector3 source, Vector3 target)
    {
        float startTime = Time.time;
        while (Time.time < startTime + 1.0f)
        {
            Player.transform.position = Vector3.Lerp(source, target, (Time.time - startTime));
            yield return null;
        }
        Player.transform.position = target;
    }


    public void SetStartPos()
    {
        StopCoroutine(coroutine);
        Player.transform.position = playerStartPos;
        Player.SetActive(true);
        

    }
}
