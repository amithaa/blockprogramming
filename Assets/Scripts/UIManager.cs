﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private static UIManager _instance;

    public static UIManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public GameObject ExecutionParent;
    public GameObject Canvas;

    //Error display
    public Text debugText;

    public void PlayProgram()
    {
        int children = ExecutionParent.transform.childCount;
        string puzzleName = "";
        for (int i = 0; i < children; ++i)
        {
            Vector3 source = Vector3.zero;
            Vector3 target = Vector3.zero;

            //print("For loop: " + ExecutionParent.transform.GetChild(i).GetComponent<PuzzlePiece>().puzzleName);
            puzzleName = "";
            puzzleName = ExecutionParent.transform.GetChild(i).GetComponent<PuzzlePiece>().puzzleName;

            if (puzzleName != "")
            {
                PlayerMovement.Instance.ExecutionList.Add(puzzleName);
            }
            
        }

        PlayerMovement.Instance.Execute();
    }



    public void ClearAll()
    {
        //foreach (Transform child in ExecutionParent.transform)
        //{
        //    if (child.transform.childCount > 0)
        //    {
        //        GameObject.Destroy(child.transform.GetChild(0).gameObject);
        //        child.transform.GetComponent<DropScript>().filled = false;
        //        child.GetComponent<PuzzlePiece>().puzzleName = "";

        //    }
        //}

        //PlayerMovement.Instance.ExecutionList.Clear();
    }



    /// <summary>
	/// Displays an error message
	/// </summary>
	public void AlertError(string errorText)
    {
        debugText.gameObject.SetActive(true);
        debugText.text = errorText;
        StartCoroutine(DisableErrorText());
    }


    /// <summary>
    /// Disables the error message text
    /// </summary>
    IEnumerator DisableErrorText()
    {
        yield return new WaitForSeconds(5.0f);
        debugText.text = "";
        debugText.gameObject.SetActive(false);

    }
}
