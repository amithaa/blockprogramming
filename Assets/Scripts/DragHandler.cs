﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public static GameObject itemBeingDragged;
	GameObject duplicateSticker;
	Vector3 startposition;

	Transform startParent;

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		duplicateSticker = Instantiate (gameObject,transform) as GameObject;
        //duplicateSticker.transform.SetParent (UIManager.Instance.ExecutionParent.transform.parent.transform);
        startParent = transform.parent;
		Destroy (duplicateSticker.GetComponent<DragHandler>());

		itemBeingDragged = duplicateSticker;    
		startposition = gameObject.transform.position;

        duplicateSticker.GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		itemBeingDragged.transform.position = Input.mousePosition;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
        itemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = true;
		if (itemBeingDragged.transform.parent && itemBeingDragged.transform.parent.name != "Slot") {
			itemBeingDragged.transform.position = startposition;
			Destroy (duplicateSticker);
		}
		itemBeingDragged = null;

		
	}

	#endregion
}
