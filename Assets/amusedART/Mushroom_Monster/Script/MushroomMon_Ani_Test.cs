﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomMon_Ani_Test : MonoBehaviour {

	public const string IDLE	= "Idle";
	public const string RUN		= "Run";
	public const string ATTACK	= "Attack";
	public const string DAMAGE	= "Damage";
	public const string DEATH	= "Death";

	Animation anim;

	void Start () {
		anim = GetComponent<Animation>();
	}

	public void IdleAni (){
		anim.CrossFade (IDLE);
	}

	public void RunAni (){
		anim.CrossFade (RUN);
	}

	public void AttackAni (){
		anim.CrossFade (ATTACK);
	}

	public void DamageAni (){
		anim.CrossFade (DAMAGE);
	}

	public void DeathAni (){
		anim.CrossFade (DEATH);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            PlayerMovement.Instance.SetStartPos();
        }

        if (collision.gameObject.tag == "WinningItem")
        {
            gameObject.SetActive(false);
            PlayerMovement.Instance.won = true;
            PlayerMovement.Instance.SetStartPos();
            UIManager.Instance.AlertError("You have found the Item! Good job!");

            MazeSpawner.Instance.ResetAll();
        }
    }
}
