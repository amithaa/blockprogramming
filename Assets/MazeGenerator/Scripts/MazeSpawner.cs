﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using System.Collections.Generic;

//<summary>
//Game object, that creates maze and instantiates it in scene
//</summary>
public class MazeSpawner : MonoBehaviour {
	public enum MazeGenerationAlgorithm{
		PureRecursive,
		RecursiveTree,
		RandomTree,
		OldestTree,
		RecursiveDivision,
	}

    private static MazeSpawner _instance;

    public static MazeSpawner Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public MazeGenerationAlgorithm Algorithm = MazeGenerationAlgorithm.PureRecursive;
	public bool FullRandom = false;
	public int RandomSeed = 12345;
	public GameObject Floor = null;
	public GameObject Wall = null;
	public GameObject Pillar = null;
    public GameObject winningItem = null;
	public int Rows = 5;
	public int Columns = 5;
	public float CellWidth = 5;
	public float CellHeight = 5;
	public bool AddGaps = true;
	public GameObject GoalPrefab = null;

	private BasicMazeGenerator mMazeGenerator = null;

    public NavMeshSurface surface;

    public Vector3 destination;

    public bool readyToFindDistance = false;

    [SerializeField]
    float pathDistance = 0;

    void Start () {
        StartMaze();
	}


    public void StartMaze()
    {

        if (!FullRandom)
        {
            Random.seed = RandomSeed;
        }
        switch (Algorithm)
        {
            case MazeGenerationAlgorithm.PureRecursive:
                mMazeGenerator = new RecursiveMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RecursiveTree:
                mMazeGenerator = new RecursiveTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RandomTree:
                mMazeGenerator = new RandomTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.OldestTree:
                mMazeGenerator = new OldestTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RecursiveDivision:
                mMazeGenerator = new DivisionMazeGenerator(Rows, Columns);
                break;
        }

        int WinningItemRow = Rows / 2;
        int WinningItemCol = Columns / 2;

        System.Random ran1 = new System.Random();
        var value1 = new List<int>{
        0,
        Rows - 1};
        int index1 = ran1.Next(value1.Count);
        int playerx = value1[index1];

        System.Random ran2 = new System.Random();
        var value2 = new List<int>{
        0,
        Columns - 1};
        int index2 = ran1.Next(value2.Count);
        int playery = value2[index2];
        
        mMazeGenerator.GenerateMaze();
        for (int row = 0; row < Rows; row++)
        {
            for (int column = 0; column < Columns; column++)
            {
                float x = column * (CellWidth + (AddGaps ? .2f : 0));
                float z = row * (CellHeight + (AddGaps ? .2f : 0));
                MazeCell cell = mMazeGenerator.GetMazeCell(row, column);
                GameObject tmp;
                tmp = Instantiate(Floor, new Vector3(x, 0, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                tmp.transform.parent = transform;
                
                if (row == playerx && column == playery)
                {
                    Vector3 target = new Vector3(x, 0 , z);
                    PlayerMovement.Instance.Player.transform.position = target;
                    PlayerMovement.Instance.playerStartPos = PlayerMovement.Instance.Player.transform.position;
                }
                if (cell.WallRight)
                {
                    tmp = Instantiate(Wall, new Vector3(x + CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject;// right
                    tmp.transform.parent = transform;
                }
                if (cell.WallFront)
                {
                    tmp = Instantiate(Wall, new Vector3(x, 0, z + CellHeight / 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;// front
                    tmp.transform.parent = transform;
                }
                if (cell.WallLeft)
                {
                    tmp = Instantiate(Wall, new Vector3(x - CellWidth / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 270, 0)) as GameObject;// left
                    tmp.transform.parent = transform;
                }
                if (cell.WallBack)
                {
                    tmp = Instantiate(Wall, new Vector3(x, 0, z - CellHeight / 2) + Wall.transform.position, Quaternion.Euler(0, 180, 0)) as GameObject;// back
                    tmp.transform.parent = transform;
                }
                //if(cell.IsGoal && GoalPrefab != null){
                //	tmp = Instantiate(GoalPrefab,new Vector3(x,1,z), Quaternion.Euler(0,0,0)) as GameObject;
                //	tmp.transform.parent = transform;
                //}

                if (row == WinningItemRow && column == WinningItemCol)
                {
                    tmp = Instantiate(winningItem, new Vector3(x, 1, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                    tmp.transform.parent = transform;
                    destination = tmp.transform.position;
                }
            }
        }
        if (Pillar != null)
        {
            for (int row = 0; row < Rows + 1; row++)
            {
                for (int column = 0; column < Columns + 1; column++)
                {
                    float x = column * (CellWidth + (AddGaps ? .2f : 0));
                    float z = row * (CellHeight + (AddGaps ? .2f : 0));
                    GameObject tmp = Instantiate(Pillar, new Vector3(x - CellWidth / 2, 0, z - CellHeight / 2), Quaternion.identity) as GameObject;
                    tmp.transform.parent = transform;
                }
            }
        }
        surface.BuildNavMesh();

        NavMeshAgent agent = PlayerMovement.Instance.Player.GetComponent<NavMeshAgent>();
        agent.enabled = true;
        //agent.ResetPath();
        agent.SetDestination(destination);
        agent.isStopped = true;

        readyToFindDistance = true;
    }


    private void Update()
    {
        if (readyToFindDistance)
        {
            if (PlayerMovement.Instance.Player.GetComponent<NavMeshAgent>().hasPath)
            {
                readyToFindDistance = false;
                pathDistance = PathLength(PlayerMovement.Instance.Player.GetComponent<NavMeshAgent>().path);
                print(pathDistance.ToString());

                if (pathDistance > 20.0f)
                {
                    ResetAll();
                } 
                return;
            } else
            {
                print("No path!");
            }
        }
    }


    float PathLength(NavMeshPath path)
    {
        if (path.corners.Length < 2)
            return 0;

        Vector3 previousCorner = path.corners[0];
        float lengthSoFar = 0.0F;
        int i = 1;
        while (i < path.corners.Length)
        {
            Vector3 currentCorner = path.corners[i];
            lengthSoFar += Vector3.Distance(previousCorner, currentCorner);
            previousCorner = currentCorner;
            i++;
        }
        return lengthSoFar;
    }



    public void ResetAll()
    {
        PlayerMovement.Instance.Player.GetComponent<NavMeshAgent>().enabled = false;

        for (int i = 0; i < transform.childCount; ++i)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        PlayerMovement.Instance.won = false;
        UIManager.Instance.ClearAll();
        StartMaze();
    }
}
